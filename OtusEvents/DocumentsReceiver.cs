﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace OtusEvents
{
    public class DocumentsReceiver
    {
        private readonly List<string> names = new List<string>();
        private readonly List<string> files = new List<string>();

        public event Action DocumentsReady;
        public event Action TimedOut;

        private FileSystemWatcher watcher;
        private Timer timer;

        public DocumentsReceiver(params string[] listOfDocs)
        {
            names.AddRange(listOfDocs);
        }
        public void Start(string targetDirectory, double waitingInterval)
        {
            watcher = new FileSystemWatcher(targetDirectory);
            watcher.EnableRaisingEvents = true;
            watcher.Created += Watcher_Created;
            watcher.Deleted += Watcher_Deleted;

            TimedOut += () => Console.WriteLine("Время истекло");
            DocumentsReady += () => Console.WriteLine("Документы загружены");

            timer = new Timer(waitingInterval);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();            
        }

        private void Watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            if (names.Contains(e.Name))
            {
                files.Remove(e.Name);
            }
        }


        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            if (names.Contains(e.Name) && !files.Contains(e.Name))
            {
                files.Add(e.Name);

                if (files.Count == names.Count)
                {
                    DocumentsReady?.Invoke();
                    Unsubscribe();
                }
            }
        }

        private void Unsubscribe()
        {
            timer.Elapsed -= Timer_Elapsed;
            watcher.Created -= Watcher_Created;
            watcher.Deleted -= Watcher_Deleted;
            watcher.Dispose();
            timer.Dispose();
        }
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimedOut?.Invoke();
            Unsubscribe();
        }
    }
}
