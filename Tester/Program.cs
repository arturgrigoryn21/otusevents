﻿using System;
using OtusEvents;
using System.IO;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            var receiver = new DocumentsReceiver("Паспорт.jpg", "Заявление.txt", "Фото.jpg");
            var dir = Directory.CreateDirectory(@"F:\Test");
            receiver.Start(dir.FullName, 1000*500);
            Console.ReadLine();
        }
    }
}
